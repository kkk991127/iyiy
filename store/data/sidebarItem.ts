export default [
  {
    isTitle: true,
    name: "Menu",
    url: "",
    icon: "",
    submenu: [],
  },

  {
    isTitle: false,
    name: "강사 대시보드",
    url: "/",
    icon: "user",
    submenu: [],
  },
  {
    isTitle: true,
    name: "학생관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "과정명 선택",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "훈련생 현황",
        url: "/studentinfo/studentlist",
        icon: "chevron-right",
        submenu: [
        ],
      },
      {
        isTitle: false,
        name: "개인정보",
        url: "/studentinfo/studentid",
        icon: "chevron-right",
        submenu: [
        ],
      },
      {
        isTitle: false,
        name: "개인정보 등록",
        url: "",
        icon: "edit-2",
        submenu: [
        ],
      },
      {
        isTitle: false,
        name: "개인정보 등록",
        url: "",
        icon: "edit-2",
        submenu: [
        ],
      },
      {
        isTitle: false,
        name: "취업관리",
        url: "",
        icon: "users",
        submenu: [],
      },
      {
        isTitle: false,
        name: "상담관리",
        url: "/advice/list",
        icon: "users",
        submenu: [],
      },
    ],
  },
  {
    isTitle: true,
    name: "훈련관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "과정명선택",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "훈련일지 등록",
        url: "",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "훈련일지",
        url: "",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "출결관리",
        url: "",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: true,
    name: "평가관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "과정명선택",
    key: "forms",
    icon: "list",
    submenu: [
      {
        isTitle: false,
        name: "과목현황",
        url: "",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "사전평가",
        url: "",
        icon: "chevron-right",
        submenu: [
        ],
      },
    ],
  },
  {
    isTitle: true,
    name: "설문관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "과정명선택",
    key: "forms",
    icon: "list",
    submenu: [
        {
      isTitle: false,
      name: "",
      url: "",
      icon: "chevron-right",
      submenu: [],
    }

    ],
  },
  {
    isTitle: true,
    name: "게시판 관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "과정명선택",
    key: "forms",
    icon: "list",
    submenu: [
      {
        isTitle: false,
        name: "",
        url: "",
        icon: "chevron-right",
        submenu: [],
      }

    ],
  },

  {
    isTitle: false,
    name: "Components",
    key: "component",
    icon: "box",
    submenu: [
      {
        isTitle: false,
        name: "Accordion",
        url: "/components/accordion",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Button",
        url: "/components/button",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Modal",
        url: "/components/modal",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Offcanvas",
        url: "/components/offcanvas",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Dropdown",
        url: "/components/dropdown",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Dialog",
        url: "/components/dialog",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Toast",
        url: "/components/toast",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Icons",
        url: "/components/icon",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Skeleton",
        url: "/components/skeleton",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Tabs",
        url: "/components/tabs",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  //{
  //  isTitle: false,
   // name: "Inbox",
  //  url: "/inbox",
  //  icon: "inbox",
   // submenu: [],
  //},

  {
    isTitle: true,
    name: "Form & Tables",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "Forms",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "General",
        url: "/form/general",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: false,
    name: "Table",
    key: "table",
    icon: "table",
    submenu: [
      {
        isTitle: false,
        name: "Datatable Clientside",
        url: "/table/simple",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Datatable Serverside",
        url: "/table/datatable",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },

  {
    isTitle: true,
    name: "Pages",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "Page",
    url: "",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "Login",
        url: "/login",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Register",
        url: "/register",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Profile",
        url: "/profile",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
];
